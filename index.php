<? require("template/header.php"); ?>
<div class="b-slider-row row pb-4 mx-0">
    <div class="container">
        <div class="b-title _title-white col-12">Моя <span>супер команда</span></div>
        <? require("template/slider.php"); ?>
    </div>
</div>
<div class="b-form-row row pb-4 mx-0">
    <div class="container">
        <div class="b-title _title-dark col-12 justify-content-center">Добавь своего <span> героя</span></div>
        <form enctype="multipart/form-data" class="b-add-form row justify-content-center">
            <div class="col-sm-12 col-md-6 col-xl-4">
                <label for="add-form__name">Имя <span>*</span></label>
                <input type='text' id="add-form__name" name="Name" required>
            </div>
            <div class="col-sm-12 col-md-6 col-xl-4">
                <label for="add-form__rank">Титул <span>*</span></label>
                <input type='text' id="add-form__rank" name="Rank" required>
            </div>
            <div class="col-sm-12 col-md-12 col-xl-8">
                <label for="add-form__photo">Фото <span>*</span></label>
                <div class="add-form__photo" id="photoDropZone"><span>Чтобы добавить фото героя перетащите изображение в это поле или просто
                        кликните сюда</span>
                </div>
                <input id="add-form__photo" type="file" accept="image/*" name="Photo" required>
            </div>
            <div class="col-sm-12 col-md-12 col-xl-8 d-flex justify-content-center justify-content-md-end">
                <input type="submit" value="ПРИНЯТЬ">
            </div>
        </form>
        <div class="b-success">
            <div id="successMessage"></div>
            <div id="resume">Продолжить?</div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $(".b-heroes-slider").slick({
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoPlay: true,
            arrows: false,
            appendDots: $('.b-heroes-slider__pagination'),
            dotsClass: 'b-heroes-slider__pagination',
            cssEase: 'ease-in-out',
            speed: 1000,
            responsive: [
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 770,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        speed: 600
                    }
                },
                {
                    breakpoint: 550,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        speed: 400
                    }
                }
            ]
        });
    });
</script>
<? require("template/footer.php"); ?>
