$(document).ready(function () {
//Отправка формы на сервер
    var mainForm = $('.b-add-form'),
        blockSuccess = $('.b-success'),
        successMessage = $('#successMessage');
    mainForm.on('submit', function (e) {
        e.preventDefault();
        var formData = new FormData($(this).get(0));
        $.ajax({
            type: "POST",
            url: "template/uploadToBd.php",
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (results) {
                mainForm.fadeOut(300, function () {
                    if (results.InBase === true) {
                        successMessage.html('Эй! Ты уже и так в команде, <span>' + results.Name + '</span> !');
                        blockSuccess.fadeIn(300);
                    }
                    else {
                        successMessage.html('Добро пожаловать в команду, <span>' + results.Name + '</span> !');
                        blockSuccess.fadeIn(300);
                        $('.b-heroes-slider').slick('slickAdd',
                            '<div class="b-heroes-slider__unit"><div class="__photo" style="background-image: url(template/img/user_img/' + results.Photo + ');"></div><div class="__name">' + results.Name + '</div><div class="__rank">' + results.Rank + '</div> <div class="__enter-date">Дата вступления в команду: <span>' + results.Date + '</span></div></div>');
                    }
                });
            },
            error: function () {
                alert('Данные введены не корректно. Попробуйте еще раз!');
            }
        });
    });
//Дропзона
    var dropZone = $('#photoDropZone'),
        dropZoneText = $('.add-form__photo span'),
        hidenFileInput = $('#add-form__photo'),
        maxPhotoSize = 500000;
    var checkFile = function (file) {
        if (file.size > maxPhotoSize) {
            dropZoneText.html('Файл слишком большой для загрузки!Попробуйте другой файл');
            dropZone.removeClass('drop').addClass('error');
        }
        else if (!(file.type.substring(0, 6) === 'image/')) {
            dropZoneText.html('Это не файл изображения! Попробуйте другой файл');
            dropZone.removeClass('drop').addClass('error');
        }
        else {
            dropZoneText.html('То, что нужно! Ваш файл ' + file.name + ' готов к загрузке!');
            dropZone.removeClass('error').addClass('drop');
        }
    };
    dropZone.on('dragover', function () {
        dropZone.addClass('dragover');
        return false;
    });
    dropZone.on('dragleave', function () {
        dropZone.removeClass('dragover');
        return false;
    });
    dropZone.on('drop', function (e) {
        e.preventDefault();
        dropZone.removeClass('dragover error').addClass('drop');
        var file = e.originalEvent.dataTransfer.files[0];
        document.getElementById('add-form__photo').files = e.originalEvent.dataTransfer.files;
        checkFile(file);
    });
    dropZone.on('click', function () {
        hidenFileInput.click();
    });
    hidenFileInput.on('change', function () {
        var file = this.files[0];
        checkFile(file);
    });
    //Повторное заполнение формы
    var resumeBtn = $('#resume');
    resumeBtn.on('click',function () {
        blockSuccess.fadeOut(300,function () {
            mainForm[0].reset();
            document.getElementById('add-form__photo').files = undefined;
            dropZoneText.html('Чтобы добавить фото героя перетащите изображение в это поле или просто кликните сюда');
            dropZone.removeClass('drop').removeClass('error');
            mainForm.fadeIn(300);
        });
    });
});


