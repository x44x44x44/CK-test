<?php

$host = 'localhost';
$database = 'cx14412_test';
$user = 'cx14412_test';
$pswd = '1234567890';

if (!empty($_POST["Name"]) && !(strlen(preg_replace('/\s+/u', '', $_POST["Name"])) === 0) && !empty($_POST["Rank"]) && !(strlen(preg_replace('/\s+/u', '', $_POST["Rank"])) === 0)) {
    $Name = $_POST['Name'];
    $Rank = $_POST['Rank'];
    header('Content-type: application/json');
    $newPhotoName = rand(0, 2000) . $_FILES['Photo']['name'];
    $currentDate = date("d.m.Y");
    checkBd($Rank, $Name, $currentDate, $newPhotoName);
} else {
    echo '';
}
function addToBd($pdo,$Name, $Rank, $newPhotoName, $currentDate)
{
    $stmt = $pdo->prepare("INSERT INTO heroes (Name, Rank, Date, Photo) VALUES (:name,:rank,:date,:photo)");
    $stmt->bindValue(':rank', $Rank, PDO::PARAM_STR);
    $stmt->bindValue(':name', $Name, PDO::PARAM_STR);
    $stmt->bindValue(':date', $currentDate, PDO::PARAM_STR);
    $stmt->bindValue(':photo', $newPhotoName, PDO::PARAM_STR);
    $stmt->execute();
    $pdo=NULL;
}
function checkBd($Rank, $Name, $currentDate, $newPhotoName)
{
    $host = '127.0.0.1';
    $database   = 'test';
    $user = 'root';
    $password = '';
    $charset = 'utf8';

    $dsn = "mysql:host=$host;dbname=$database;charset=$charset";
    $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    $pdo = new PDO($dsn, $user, $password, $opt);

    $sthChosen = $pdo->prepare("SELECT * FROM heroes WHERE Rank = :rank AND Name = :name");
    $sthChosen->bindValue(':rank', $Rank, PDO::PARAM_STR);
    $sthChosen->bindValue(':name', $Name, PDO::PARAM_STR);
    $sthChosen->execute();
    $numRowsChosen = $sthChosen -> rowCount();

    $answer = ($numRowsChosen > 0 ? true : false);
    $Data = [
        'Name' => $Name,
        'Rank' => $Rank,
        'Photo' => $newPhotoName,
        'Date' => $currentDate,
        'InBase' => $answer
    ];
    if ($answer === false) {
        move_uploaded_file($_FILES['Photo']['tmp_name'], 'img/user_img/' . $newPhotoName);
        addToBd($pdo,$Name, $Rank, $newPhotoName, $currentDate);
        echo json_encode($Data);
    } else {
        echo json_encode($Data);
    }
}

?>