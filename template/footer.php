<div class="b-footer">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="b-copyright col-sm-12 col-md-7">
                ALL RIGHTS RESERVED. COPYRIGHT © <span>CKDIGITAL</span>
            </div>
            <div class="b-social-icons col-sm-12 col-md-5">
                <a href="#" class="__elem" style="background-image: url(template/img/facebook.png)"></a>
                <a href="#" class="__elem" style="background-image: url(template/img/twitter.png)"></a>
                <a href="#" class="__elem" style="background-image: url(template/img/google.png)"></a>
                <a href="#" class="__elem" style="background-image: url(template/img/linkedin.png)"></a>
            </div>
        </div>
    </div>
</div>
<script src="template/js/main.js"></script>
</body>
</html>