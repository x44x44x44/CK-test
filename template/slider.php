<div class="col-12">
    <div class="b-heroes-slider">
        <?php
        $host = '127.0.0.1';
        $database = 'test';
        $user = 'root';
        $password = '';
        $charset = 'utf8';

        $dsn = "mysql:host=$host;dbname=$database;charset=$charset";
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $pdo = new PDO($dsn, $user, $password, $opt);
        $stmt = $pdo->query("SELECT * FROM heroes");
        while ($row = $stmt->fetch()) {
            echo '<div class="b-heroes-slider__unit"><div class="__photo" style="background-image: url(template/img/user_img/' . $row['Photo'] . ');"></div><div class="__name">' . $row['Name'] . '</div><div class="__rank">' . $row['Rank'] . '</div> <div class="__enter-date">Дата вступления в команду: <span>' . $row['Date'] . '</span></div></div>';
        }
        $pdo = NULL;
        ?>
    </div>
    <div class="b-heroes-slider__pagination"></div>
</div>